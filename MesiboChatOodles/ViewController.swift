//
//  ViewController.swift
//  MesiboChatOodles
//
//  Created by Ravi Ranjan on 02/04/20.
//  Copyright © 2020 Ravi Ranjan. All rights reserved.
//

import UIKit


class ViewController: UIViewController {
    
    @IBOutlet weak var messageTV: UITextView!
    
    
    @IBOutlet weak var chaiInputBoxView: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.chaiInputBoxView.layer.cornerRadius = 5.0
        self.setupMesibo()
        
    }
    
    @IBAction func onClickSendMessageButton(_ sender: UIButton) {
        
        debugPrint("Text message Sent")
        let oppositeUser = "prince@gmail.com"
        self.sentTextMessage(with: oppositeUser, message: self.messageTV.text)
    }
    
    @IBAction func onClickVideoCall(_ sender: UIButton) {
        debugPrint("Video call started")
        self.mesibocalFunction(with: false, Video: true)
        
    }
    
    @IBAction func onClickAudioCall(_ sender: UIButton) {
        debugPrint("Audio call started")
        self.mesibocalFunction(with: false, Video: false)
    }
    
    
    fileprivate func mesibocalFunction(with Incoming : Bool, Video : Bool){
        let mesiboCall = MesiboCall.sharedInstance()
        mesiboCall?.getConfig()
        let oppositeUser = "prince@gmail.com"
        
        let mid : UInt32 = (Mesibo.getInstance()?.random())!
        
        mesiboCall?.call(self, callid: mid, address: oppositeUser, video: Video, incoming: Incoming)
        
    }
    
}

extension ViewController: MesiboDelegate {
    
    fileprivate func setupMesibo(){
        Mesibo.getInstance()?.addListener(self)
        /*
         user  Detail
         
         email : ravi@gmail.com
         Auth Token : bbf917af8d1df1dba5a1bbdeb44ae73cc565efaab6b78449feee12745f
         
         email : prince@gmail.com
         Auth Token : 1ae93955c56d5911db908db2fa1bae97cfd9ada436356c412e6612745e
         
         */
        
        
        Mesibo.getInstance()?.setAccessToken("bbf917af8d1df1dba5a1bbdeb44ae73cc565efaab6b78449feee12745f")
        Mesibo.getInstance()?.start()
    }
    
    func mesibo_(onConnectionStatus status: Int32) {
        print("connection status is \(status)")
    }
    
    func mesibo_(onMessage params: MesiboParams!, data: Data!) {
        debugPrint("You will receive message her")
    }
    func mesibo_(onMessageStatus params: MesiboParams!) {
        debugPrint("you will see messgae change status here")
    }
    
    
    fileprivate func sentTextMessage (with to : String, message : String){
        let params = MesiboParams()
        params.peer = to
        params.flag = UInt32(MESIBO_FLAG_READRECEIPT | MESIBO_FLAG_DELIVERYRECEIPT)
        let mid : UInt32 = (Mesibo.getInstance()?.random())!
        Mesibo.getInstance()?.sendMessage(params, msgid: mid, string: message)
        
    }
}
